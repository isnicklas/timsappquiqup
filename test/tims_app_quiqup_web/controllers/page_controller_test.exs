defmodule TimsAppQuiqupWeb.PageControllerTest do
  use TimsAppQuiqupWeb.ConnCase, async: false
  import Mock
  alias TimsAppQuiqup.DataService

  describe "GET index/2" do
    test "successful response", %{conn: conn} do
      conn = get conn, page_path(conn, :index)

      assert html_response(conn, 200)
    end
  end

  describe "GET data_feed/2" do
    test "successful response", %{conn: conn} do
      data = [%{
        coordinates: "-.086652,51.510844",
        severity: "Moderate",
        status: "Active"
      }]

      expected_data = {:ok, data}

      with_mock DataService, [get_data: fn -> expected_data end] do
        conn = get conn, page_path(conn, :data_feed)

        assert json_response(conn, 200)
      end
    end

    test "unsuccessful data fetch returns 422", %{conn: conn} do
      expected_data = {:error, "something freaked out"}

      with_mock DataService, [get_data: fn -> expected_data end] do
        conn = get conn, page_path(conn, :data_feed)

        assert json_response(conn, 422)
      end
    end
  end
end
