defmodule TimsAppQuiqup.FakeData do
  @body """
  <Root xmlns="http://www.tfl.gov.uk/tims/1.0">
    <Header>
        <Identifier>Transport for London | Live Traffic Disruptions</Identifier>
        <DisplayTitle>Transport for London | Traffic Information Management System (TIMS) Feed</DisplayTitle>
        <Version>2.0</Version>
        <PublishDateTime canonical="2018-03-29T21:24:00Z">Thursday, 29 Mar 2018 22:24:00 +01:00</PublishDateTime>
        <Author>digital@tfl.gov.uk</Author>
        <Owner>Transport for London</Owner>
        <RefreshRate>5</RefreshRate>
        <Max_Latency>30</Max_Latency>
        <TimeToError>30</TimeToError>
        <Schedule>Every 5 minutes</Schedule>
        <Attribution>
            <Url>http://www.tfl.gov.uk/</Url>
            <Text>(c) Transport for London</Text>
            <Logo>http://www.tfl.gov.uk/tfl-global/images/roundel.gif</Logo>
        </Attribution>
        <Language>EN</Language>
    </Header>
    <Disruptions>
      <Disruption id='183717'>
          <status>Active</status>
          <severity>Moderate</severity>
          <levelOfInterest>High</levelOfInterest>
          <category>Works</category>
          <subCategory>Emergency</subCategory>
          <startTime>2018-02-24T00:00:00Z</startTime>
          <endTime>2018-04-06T17:00:00Z</endTime>
          <location>[A10] Gracechurch Street (EC3V,EC4R) (City Of London)</location>
          <corridor>Bishopsgate Cross Route</corridor>
          <comments>[A10] Gracechurch Street (EC3V) (Northbound) at the junction of King William Street - Road closed to facilitate emergency gas works. Additionally, access to London Bridge northbound is restricted to buses, taxis and cycles at the junction of Duke Street Hill. Cannon Street is closed eastbound.</comments>
          <currentUpdate>Minor delays on Southwark Bridge and Blackfriars Bridge northbound.</currentUpdate>
          <remarkTime>2018-03-29T21:18:12Z</remarkTime>
          <lastModTime>2018-03-29T21:18:12Z</lastModTime>
          <CauseArea>
              <DisplayPoint>
                  <Point>
                      <coordinatesEN>532873.225,180844.734</coordinatesEN>
                      <coordinatesLL>-.086652,51.510844</coordinatesLL>
                  </Point>
              </DisplayPoint>
          </CauseArea>
      </Disruption>
    </Disruptions>
  </Root>
  """

  def one_response do
    {:ok, SweetXml.parse(@body)}
  end

  def error_response do
    {:error, "Something went wrong"}
  end
end
