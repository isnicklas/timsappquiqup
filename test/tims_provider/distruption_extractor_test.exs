defmodule TimsAppQuiqup.DistruptionExtractorTest do
  use ExUnit.Case, async: false
  import Mock
  alias TimsAppQuiqup.{Connector, FakeData, DisruptionExtractor}

  describe "present_data/0" do
    test "when there is an available distruption data" do
      expected_data = [%{
        coordinates: "-.086652,51.510844",
        severity: "Moderate",
        status: "Active"
      }]

      with_mock Connector, [get_tims_xml: &(FakeData.one_response/0)] do
        assert(
          DisruptionExtractor.present_data() == {:ok, expected_data}
        )
      end
    end

    test "when there is an error" do
      with_mock Connector, [get_tims_xml: &(FakeData.error_response/0)] do
        assert(
          DisruptionExtractor.present_data() == {:error, "Something went wrong"}
        )
      end
    end
  end
end
