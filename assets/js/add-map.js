export default {
  initMap() {
    this.asyncloadDistruptions().then((data) => {
      const map = new google.maps.Map(this.mapId, {
        zoom: 15,
        center: new google.maps.LatLng(51.508742, -0.120850)
      })

      this.plotMarkers(data, map)
    })
  },
  plotMarkers(data, map) {
    const coordinates = data.map((distruption) => {
      const [long, lat] = distruption.coordinates.split(",")

      return {lat, long}
    })

    coordinates.forEach((coord) => {
      const marker = new google.maps.Marker({
        position: new google.maps.LatLng(coord.lat, coord.long),
        map: map
      });
    })
  },
  asyncloadDistruptions() {
    return new Promise(resolve => {
      this.loadDistruptions(resolve)
    })
  },
  loadDistruptions(callback) {
    const xhttp = new XMLHttpRequest()

    xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        callback(JSON.parse(this.response))
      }
    }

    xhttp.open('GET', '/data_feed', true)
    xhttp.send()
  },
  distruptions: [],
  mapId: document.getElementById('map')
}
