defmodule TimsAppQuiqup.DataService do
  @moduledoc """
    This module starts the GenServer for pooling data and serving data
    to clients
  """
  alias TimsAppQuiqup.{Server, DisruptionExtractor}

  def start_link do
    GenServer.start_link(Server, :ok, name: Server)
  end

  @doc """
    Responsible for refetching the TIM data
  """
  def hard_refresh_data do
    response = DisruptionExtractor.present_data()

    GenServer.cast(Server, {:save, response})
  end

  @doc """
    Responsible for serving stored TIM data
  """
  def get_data do
    GenServer.call(Server, :get)
  end
end
