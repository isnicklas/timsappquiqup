defmodule TimsAppQuiqup.Connector do
  @moduledoc """
    This Module is for connecting to tfl
  """

  def get_tims_xml do
    request()
    |> HTTPoison.get()
    |> parse_body()
  end

  defp parse_body({:ok, %HTTPoison.Response{
    body: body, headers: _list, request_url: _term, status_code: status
  }}) do
    case status do
      200 -> {:ok, body}
      _ -> {:error, "Something went wrong"}
    end
  end

  defp parse_body({:error, _reson}) do
    {:error, "Something went wrong"}
  end

  defp request do
    "#{url()}?app_id=#{app_id()}?app_key=#{app_key()}"
  end

  defp app_id do
    Application.get_env(:tims_app_quiqup, :app_id)
  end

  defp app_key do
    Application.get_env(:tims_app_quiqup, :app_key)
  end

  defp url do
    "#{Application.get_env(:tims_app_quiqup, :url)}/syndication/feeds/tims_feed.xml"
  end
end
