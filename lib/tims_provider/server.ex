defmodule TimsAppQuiqup.Server do
  use GenServer
  alias TimsAppQuiqup.{DisruptionExtractor, DataService}

  @pool_time 30000

  def init(:ok) do
    Task.start(fn ->
      DataService.hard_refresh_data()

      schedule_pool()
    end)

    {:ok, {}}
  end

  def handle_call(:get, _from, state) do
    {:reply, state, state}
  end

  def handle_cast({:save, data}, _state) do
    {:noreply, data}
  end

  def handle_info(:pool_data, _state) do
    data = DisruptionExtractor.present_data()

    schedule_pool()

    {:noreply, data}
  end

  defp schedule_pool do
    Process.send_after(__MODULE__, :pool_data, @pool_time)
  end
end
