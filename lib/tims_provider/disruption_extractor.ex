defmodule TimsAppQuiqup.DisruptionExtractor do
  @moduledoc """
    This module extracts the relevant data needed
    severity, coordinates, location, category
  """

  import SweetXml
  alias TimsAppQuiqup.Connector

  def present_data do
    Connector.get_tims_xml()
    |> extract()
  end

  defp extract({:error, reason}), do: {:error, reason}
  defp extract({:ok, xml_data}) do
    xml_data
    |> xpath(~x"//Disruption"l)
    |> extract_required_data()
  end

  defp extract_required_data([]), do: {:ok, []}
  defp extract_required_data(distruptions) do
    data = Enum.map(distruptions, &attributes/1)

    {:ok, data}
  end

  defp attributes(distruption_data) do
    data = xmap(
      distruption_data,
      status: ~x"//status/text()",
      severity: ~x"//severity/text()",
      coordinates: ~x"//coordinatesLL/text()"
    )

    stringify_map_values(data)
  end

  defp stringify_map_values(data) do
    Enum.reduce(
      data,
      %{},
      fn {key, value}, acc -> Map.put(acc, key, to_string(value)) end
    )
  end
end
