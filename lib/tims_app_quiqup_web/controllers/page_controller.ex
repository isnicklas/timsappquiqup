defmodule TimsAppQuiqupWeb.PageController do
  use TimsAppQuiqupWeb, :controller

  alias TimsAppQuiqup.DataService

  def index(conn, _params) do
    render conn, "index.html"
  end

  def data_feed(conn, _params) do
    case DataService.get_data do
      {:ok, data} -> json(conn, data)
      _ ->
      conn
      |> put_status(422)
      |> json(%{errors: "Something went wrong"})
    end
  end
end
