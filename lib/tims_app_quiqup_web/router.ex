defmodule TimsAppQuiqupWeb.Router do
  use TimsAppQuiqupWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", TimsAppQuiqupWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/data_feed", PageController, :data_feed
  end

  # Other scopes may use custom stacks.
  # scope "/api", TimsAppQuiqupWeb do
  #   pipe_through :api
  # end
end
