# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :tims_app_quiqup, TimsAppQuiqupWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8nwm9v1BEZRJ/e5mjk5W6dpNBelFXLaRfhd9Y4OAJWHzYvX6IjY+8DmsCuhvDVYk",
  render_errors: [view: TimsAppQuiqupWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: TimsAppQuiqup.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# TFL Connector config

config :tims_app_quiqup,
  url: "https://data.tfl.gov.uk/tfl/",
  app_id: "",
  app_key: ""
# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
