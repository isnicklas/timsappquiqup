# TimsAppQuiqup

A simple app to show live TIMS data.
You will need an app_key and app_id from TFL in order to
get some data.

You will also need google api key.

The app currently polls data every 30 seconds.


### Configure TimsAppQuiqup

- Open `config.exs` file in config and
  insert tfl `app_id` and `app_key`  

- Open the layout template. Find the google script tag
  and insert google api key.

### To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
